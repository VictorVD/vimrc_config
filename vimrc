" --------------------------------------------------------  МЕНЕДЖЕР ПЛАГИНОВ  
set nocompatible " несовместимость с vi, необходимо для работы

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim-Plug config
let g:plug_timeout=180

call plug#begin('~/.vim/plugged')
" YouCompleteMe - автодополнение кода
Plug 'Valloric/YouCompleteMe'

" Python-mode проверка качества кода Python
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }

" C++ улучшенная подстетка кода
Plug 'octol/vim-cpp-enhanced-highlight'

" TagBar - навигатор методов и классов
Plug 'majutsushi/tagbar'

" NerdTree - дерево проекта
Plug 'scrooloose/nerdtree'

" YCM-Generator - генератор конфига для плагина автодополнения
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}

" Пакет тем
Plug 'rafi/awesome-vim-colorschemes'

" Подсветка html/xml тегов
Plug 'Valloric/MatchTagAlways'

" LaTeX 
Plug 'lervag/vimtex'

" Markdown
Plug 'shime/vim-livedown'

" AsyncRun
Plug 'skywind3000/asyncrun.vim'

Plug 'sheerun/vim-polyglot'

if has('nvim')
"   Plug 'KeitaNakamura/highlighter.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'norcalli/nvim-colorizer.lua'
endif

call plug#end()

" ------------------------------------------------------- НАСТРОЙКИ РЕДАКТОРА
set encoding=utf-8

if has("syntax")
  set foldmethod=syntax

  if !has('nvim')
    set term=screen-256color
    set background=dark
  else
    " Для плагина nvim colorizer
    set termguicolors
    lua require'colorizer'.setup()
    set guicursor=
    " autocmd BufRead *.py :colorscheme materialbox | :syntax on
    " autocmd BufNewFile,BufRead *.py let g:tmp_color=g:colors_name
    " autocmd BufEnter *.py :colorscheme gruvbox | syntax on
    " autocmd BufLeave *.py :execute 'colorscheme '.g:tmp_color
  endif
  " Цветовая схема по-умолчанию
  colorscheme PaperColor
  syntax on

endif

if has("autocmd")
  filetype plugin indent on
  " открыть файл на той же строке что и закрыл и переместиться в середину
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | exe "normal! zz" | endif
endif

if has("gui_running") 
  " размеры окна оконной версии vim
  set lines=24 columns=87
  " никаких панелей
  set guioptions-=m
  set guioptions-=T
  set guioptions-=r
  set guioptions-=L
  colorscheme CustomPaperColor
endif

" сохранение истории изменений
if version >= 700
  set undodir=~/.vim/undodir
  set undofile
  set undolevels=1000
  set undoreload=10000
endif 

" Команды пользователя
" command RelName :echo expand("%") " полезные команды

set pumheight=15 " Высота выпадающего окна автокомпиляции
"set signcolumn=yes " колонка знаков(ошибка, предупреждение...) всегда видна
set winfixheight " Сделать размер превью окна постоянным
set previewheight=7
set exrc " Поиск локалььных .vimrc, для конкретных проектов
set secure
set cursorline " Подсветка текущей строки
set number " Нумерация строк

set keymap=russian-jcukenwin " Русская раскладка
set iminsert=0 " Английский по-умолчанию 
set imsearch=0
" Подсветка курсора при отличной от английской раскладки
autocmd BufRead * highlight lCursor guifg=NONE guibg=Cyan 

set wildmenu " Отображение всех опций в статус линии
set path+=** " добавление файлов из поддиректорий в поиск
set printencoding=koi8-r " Параметры печати
set printfont=terminus
set expandtab " Замена табуляции пробелами
set tabstop=2
set shiftwidth=2

" Строка состояния всегда видна
set statusline=%<%1*\ %t\ %y\ %0*%h%m%r\ \ %a\ %=0x%B\ line:%l,\ \ col:%c\ %P
set laststatus=2
set linebreak " Перенос слов целиком
set showmatch " Показывать первую парную скобку после ввода второй
set showcmd " Дополнение команд
set splitbelow " Разбивать окно горизонтально снизу
set splitright " Разбивать окно горизонтально сверху
set listchars=tab:│─,space:·,eol:¬ " Отображение непечатных символов командой
set history=100 " Размерность истории команд
set fileencodings=utf-8,cp1251,ucs-bom,koi8-r,cp866,latin1 " Кодировка файлов

" ------------------------------------------- ДОПОЛНИТЕЛЬНОЕ МЕНЮ С КОМАНДАМИ
" Правописание
set wcm=<C-Z>
menu SpellLang.RU_EN  :setlocal spell spelllang=ru,en <CR>
menu SpellLang.off    :setlocal nospell               <CR>
menu SpellLang.RU     :setlocal spell spelllang=ru    <CR>
menu SpellLang.EN     :setlocal spell spelllang=en    <CR>
map  :emenu SpellLang.
" Форматирование
menu Format.JSON :%!python -m json.tool <CR>
menu Format.XML  :%!xmllint --format - <CR>
menu Format.GO   :%!gopls format % <CR>
map  :emenu Format.
" Просмотр
menu ViewTex.PDF :AsyncRun!zathura %<.pdf <CR>
menu ViewTex.PS :AsyncRun!zathura %<.ps <CR>
map :emenu ViewTex.

" -------------------------------------- КОМБИНАЦИИ ДЛЯ ВСТРОЕННЫХ КОМАНД VIM

" Переключение по буферам кнопками
map  <F4>      :bn <CR>
imap <F4> <Esc>:bn <CR>i
vmap <F4> <Esc>:bn <CR>

map  <F3>      :bp <CR>
imap <F3> <Esc>:bp <CR>i
vmap <F3> <Esc>:bp <CR>

" Переключение по табам
map <F7> :tabn <CR>
map <F6> :tabp <CR>

" Сохранение файлов
noremap  <silent> <F2>      :w<CR>
inoremap <silent> <F2> <Esc>:w<CR>
nnoremap <silent> <F2> <Esc>:w<CR>

noremap  <silent> <C-F2>      :wall<CR>
inoremap <silent> <C-F2> <Esc>:wall<CR>
nnoremap <silent> <C-F2> <Esc>:wall<CR>

" Запуск MAKE
noremap <F5> :make FILE=% <CR><CR> 
noremap <C-B> :make build FILE=% <CR><CR> 

" --------------------------------------------------- ФАЙЛО-ЗАВИСИМЫЕ КОМАНДЫ

" Для текстовых файлов
autocmd BufRead *.txt set textwidth=80 
autocmd BufRead *.txt set autoindent 
autocmd BufRead *.txt set colorcolumn=+1

" Выполнение кода файлов 
autocmd BufRead *.lsp map <F8> :! clear <CR> :!socat readline exec:"sbcl --noinform --load % --eval '(exit)'" <CR>
autocmd BufRead *.lua map <F8> :! clear <CR> :!lua % <CR>
autocmd BufRead *.tex map <F8> :! clear <CR> :!pdflatex % <CR>
autocmd BufRead *.rb  map <F8> :! clear <CR> :!ruby % <CR>
autocmd BufRead *.py  map <F8> :! clear <CR> :!python3 % <CR>

" -------------------------------------------------------- НАСТРОЙКА ПЛАГИНОВ 

" Настройка VIMTEX 
let g:tex_flavor = 'latex'
let g:vimtex_quickfix_mode = 0
let g:vimtex_view_method = 'zathura'

" Нфстройка SLIME LIVEDOWN для markdown
nmap gm :LivedownToggle<CR>
let g:livedown_autorun = 0 " открывать вместе с .md
let g:livedown_open = 1
let g:livedown_port = 1337
let g:livedown_browser = "firefox"

" Настройка TagBar
noremap <C-T> :TagbarToggle<CR>

" Настройка NERDTree 
map <F10> :NERDTreeToggle<CR>
" Закрывать vim если NERDTree - единственное оставшееся окно
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeNaturalSort=1
let NERDTreeIgnore=['\~$', '\.swp$[[file]]']
let NERDTreeMinimalUI=1

" NETRW
let g:netrw_banner=0
let g:netrw_liststyle=3

" Настройка YouCompleteMe
noremap <C-K> :YcmCompleter GetDoc <CR> <CR>
noremap <C-I> :AsyncRun! ctags -R -h ".py" --exclude=".*" --fields=+l <CR> : echo "Tags made" <CR>

let g:ycm_server_python_path="/usr/bin/python3"
"let g:ycm_server_python_path=system('which python3')
"let g:ycm_global_ycm_extra_conf="/usr/local/share/vim/.ycm_extra_conf.py"
"let g:ycm_extra_conf_globlist = ['!~/*']

let g:ycm_seed_identifiers_with_syntax=1 " синтаксические конструкции языка в идентификаторы
let g:ycm_min_num_of_chars_for_completion=3 " со скольки дополнять начинать
let g:ycm_autoclose_preview_window_after_insertion=1 " закрыть превью после окончания ввода
let g:ycm_collect_identifiers_from_tags_files=1 " использовать идентификаторы из файла тегов (tags)
let g:ycm_disable_for_files_larger_than_kb=0 " отмена на ограничение размера файла для автокомпиляции

" Настройка Python-mode
let g:pymode_python = 'python3'
let g:pymode_indent = 0 " автоотступ
let g:pymode_folding = 0 " свёртка средствами плагина
let g:pymode_motion = 0 " включение его комбинаций перемещений 
let g:pymode_doc = 0 " включение функции вызова документации
let g:pymode_virtualenv = 0 " автопоиск virtualenv-а
let g:pymode_run = 0 " запуск кода средствами плагина
let g:pymode_breakpoint = 0 " функцинальность для дебага
let g:pymode_lint = 1 " проверка написанного кода
let g:pymode_lint_on_write = 0 " проверка кода при сохранении с изменениями
let g:pymode_lint_unmodified = 1 " проверка при всяком сохранении
let g:pymode_lint_message = 1 " показать ошибку
let g:pymode_lint_checkers = ['pyflakes', 'pep8'] " средства проверки кода
let g:pymode_lint_cwindow = 1 " автооткрытие окна с ошибками
let g:pymode_lint_signs = 1 " показывать тип ошибки в столбце знаков
let g:pymode_rope = 0 " Отключение Rope операций рефакторинга
let g:pymode_syntax = 1 " включение подстветки синтаксиса
let g:pymode_syntax_all = 1 " подсвечивать всё
let g:pymode_syntax_slow_sync = 0 " Замедленная синхринизация подсветки
" let g:pymode_syntax_print_as_function = 1 " Подсвечивать print как функцию
" let g:pymode_syntax_highlight_async_await = 1 " Подсвечивать async/await
" let g:pymode_syntax_highlight_equal_operator = 1 " Подсвечивать знак =
" let g:pymode_syntax_highlight_stars_operator = 1 " Подсвечивать знак *
" let g:pymode_syntax_highlight_self = 1 " Подсвечивать слово self
" let g:pymode_syntax_indent_errors = 1 " Подсвечивать ошибки отступа
" let g:pymode_syntax_space_errors = 1 " Подсвечивать ошибки пробелов
" let g:pymode_syntax_string_formatting = 1 " Подсветка форматирования строк
" let g:pymode_syntax_string_format = 1     " Подсветка форматирования строк
" let g:pymode_syntax_string_templates = 1  " Подсветка форматирования строк
" let g:pymode_syntax_doctests = 1          " Подсветка форматирования строк
" let g:pymode_syntax_builtin_objs = 1 " Подсвечивать встроенные объекты языка
" let g:pymode_syntax_builtin_types = 1 " Подсвечивать встроенные типы
" let g:pymode_syntax_highlight_exceptions = 1 " Подсвечивать исключения
" let g:pymode_syntax_docstrings = 1 " Подсвечивать строки документации

" Настройка vim-cpp-enhanced-highlight
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_class_scope_highlight = 1 " подсветка классов
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_member_variable_highlight = 1 " подсветка переменных участников
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_class_decl_highlight = 1 " подсветка имён классов в объявлениях
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_posix_standard = 1 " подсветка POSIX функций
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_experimental_template_highlight = 1 " подсветка шаблонов
autocmd BufRead,BufNewFile *.cpp,*.h let g:cpp_concepts_highlight = 1 " подсветка библиотечных концептов !

" Синтаксис
autocmd BufRead,BufNewFile *.java let g:java_highlight_all=1 " Для Java
autocmd BufRead,BufNewFile *.groovy let g:groovy_highlight_all=1 " Для Groovy
autocmd BufRead,BufNewFile *.py let g:python_highlight_all = 1 " Для Python
autocmd BufRead,BufNewFile *.py let g:langOpt_python__highlight_builtins=1
