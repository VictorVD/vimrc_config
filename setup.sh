#!/usr/bin/env bash

USAGE="""\
`basename ${0}` <install|uninstall>
  install - will install this config locally
  uninstall - will uninstall this config
"""

CMD=${1:-'help'}
CONF_PATH="${HOME}/.vim"
VIMRC="vimrc"

case "${CMD}" in
  install)
    if [[ ! -d "${CONF_PATH}" ]]; then mkdir "${CONF_PATH}"; fi

    if [[ ! -f "${CONF_PATH}/${VIMRC}" ]]; then
      cp "./${VIMRC}" "${CONF_PATH}/${VIMRC}"
      echo "${VIMRC} copied to ${CONF_PATH}/${VIMRC}"
    else
      CHOICE='n'
      printf "Local vimrc already exists, rewrite(Y/n): "
      read CHOICE
      if [[ "${CHOICE}" == 'Y' ]]; then
        cp -f "./${VIMRC}" "${CONF_PATH}/${VIMRC}"
        echo "${VIMRC} force copied to ${CONF_PATH}/${VIMRC}"
      else
        mkdir -p "${CONF_PATH}/old"
        SOME_NAME="$(env LC_CTYPE='C' tr -dc '[:alpha:]' </dev/urandom | fold -w 5 | head -n1)"
        mv "${CONF_PATH}/${VIMRC}" "${CONF_PATH}/old/${SOME_NAME}"
        cp -n "./${VIMRC}" "${CONF_PATH}/${VIMRC}"
        echo "${VIMRC} safe copied to ${CONF_PATH}/${VIMRC}"
      fi
    fi
    ;;
  uninstall)
    if [[ -f "${CONF_PATH}/${VIMRC}" ]]; then
      rm "${CONF_PATH}/${VIMRC}"
      echo "${CONF_PATH}/${VIMRC} has been removed"
    else
      echo "${VIMRC} not found"
    fi
    ;;
  help|*)
    printf "${USAGE}"
    ;;
esac
